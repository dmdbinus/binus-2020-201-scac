<?php 
// load child library
require_once get_template_directory() . '/child.php';

// register our theme and the component needed
binus_child_theme('binus-2020-201-scac', array(
    'slideshow'
    ,'events'
    ,'tab'
    ,'award'
    ,'future-student'
    ,'binus-program'
    ,'combination-future-student-binus-program'
    ,'gallery'
));

binus_component_configuration(array(
    'gallery'  => array(
        'use-native-type'   => true
    )
));

// our child theme functionality
class scac{
    var $version = '1.0';

    /**
     * Class instance
     */
    static $instance;


    /**
     * Get class instance
     *
     * @since 1.0 introduced
     * @return binus the class instance
     */
    public static function get_instance(){
        if(self::$instance === null)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Template functions
     */
    function main(){
        add_action('init', array($this,'organization'));
        add_action('pre_get_posts', array($this,'organization'));
        add_action('template_redirect', array($this,'organization'));
        add_action('cmb2_admin_init', array($this,'organization'));
        add_action('rest_api_init', array($this,'service'));
    }

    function service_access($request){
        return binus()->api_access($request, 'binus-scac');
    }

    function service(){
        register_rest_route( binus()->api_namespace .'/v1', '/student-association', array(
            array(
                'methods'               => WP_REST_Server::READABLE
                ,'callback'             => function($request){
                    $args   = array(
                        'post_type'         => 'student-association'
                        ,'posts_per_page'   => $request->get_param('posts_per_page')
                        ,'post_status'      => 'publish'
                        ,'order'            => $request->get_param('order')
                        ,'orderby'          => 'title'
                    );

                    if($request->get_param('campus'))
                    {
                        $args['tax_query'] = array(
                            array(
                                'taxonomy'  => 'ukm-campus'
                                ,'field'    => 'name'
                                ,'terms'    => $request->get_param('campus')
                            )
                        );
                    }
                    
                    if($request->get_param('get_total'))
                    {
                        $args['no_found_post'] = true;

                        $query  = new WP_Query($args);

                        return rest_ensure_response(array( 'code' => 200, 'total' => $query->found_post ));
                    }
                    else
                    {
                        $query  = new WP_Query($args);

                        $clubs   = array();

                        while($query->have_posts(  ))
                        {
                            $query->the_post();

                            $clubs[] = array(
                                'name'      => get_the_title()
                                ,'website'  => get_post_meta(get_the_ID(),'_post_organization_url', true)
                                ,'logo'     => get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' )
                            );
                        }

                        return rest_ensure_response(array( 'code' => 200, 'club' => $clubs ));
                    }

                }
                ,'permission_callback'  => array($this,'service_access')
                ,'args'                 => array(
                    'posts_per_page'    => array(
                        'description'   => 'Number of feed to get'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 10
                    )
                    ,'order'            => array(
                        'description'   => 'Feed sort direction'
                        ,'type'         => 'string'
                        ,'required'     => false
                        ,'default'      => 'DESC'
                    )
                    ,'paged'    => array(
                        'description'   => 'pagination parametr'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 1
                    )
                    ,'get_total'    => array(
                        'description'   => 'Get result or the total of the feed'
                        ,'type'         => 'bool'
                        ,'required'     => false
                        ,'default'      => false
                    )
                    ,'campus'    => array(
                        'description'   => 'Filter By Campus'
                        ,'required'     => false
                        ,'default'      => false
                    )
                )
            )
        ));
        register_rest_route( binus()->api_namespace .'/v1', '/student-activity', array(
            array(
                'methods'               => WP_REST_Server::READABLE
                ,'callback'             => function($request){
                    $args   = array(
                        'post_type'         => 'studentactivityunit'
                        ,'posts_per_page'   => $request->get_param('posts_per_page')
                        ,'post_status'      => 'publish'
                        ,'order'            => $request->get_param('order')
                        ,'orderby'          => 'title'
                    );

                    if($request->get_param('campus'))
                    {
                        $args['tax_query'] = array(
                            array(
                                'taxonomy'  => 'ukm-campus'
                                ,'field'    => 'name'
                                ,'terms'    => $request->get_param('campus')
                            )
                        );
                    }
                    
                    if($request->get_param('get_total'))
                    {
                        $args['no_found_post'] = true;

                        $query  = new WP_Query($args);

                        return rest_ensure_response(array( 'code' => 200, 'total' => $query->found_post ));
                    }
                    else
                    {
                        $query  = new WP_Query($args);

                        $clubs   = array();

                        while($query->have_posts(  ))
                        {
                            $query->the_post();

                            $clubs[] = array(
                                'name'      => get_the_title()
                                ,'website'  => get_post_meta(get_the_ID(),'_post_organization_url', true)
                                ,'logo'     => get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' )
                            );
                        }

                        return rest_ensure_response(array( 'code' => 200, 'club' => $clubs ));
                    }

                }
                ,'permission_callback'  => array($this,'service_access')
                ,'args'                 => array(
                    'posts_per_page'    => array(
                        'description'   => 'Number of feed to get'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 10
                    )
                    ,'order'            => array(
                        'description'   => 'Feed sort direction'
                        ,'type'         => 'string'
                        ,'required'     => false
                        ,'default'      => 'DESC'
                    )
                    ,'paged'    => array(
                        'description'   => 'pagination parametr'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 1
                    )
                    ,'get_total'    => array(
                        'description'   => 'Get result or the total of the feed'
                        ,'type'         => 'bool'
                        ,'required'     => false
                        ,'default'      => false
                    )
                    ,'campus'    => array(
                        'description'   => 'Filter By Campus'
                        ,'required'     => false
                        ,'default'      => false
                    )
                )
            )
        ));
        register_rest_route( binus()->api_namespace .'/v1', '/student-community', array(
            array(
                'methods'               => WP_REST_Server::READABLE
                ,'callback'             => function($request){
                    $args   = array(
                        'post_type'         => 'student-community'
                        ,'posts_per_page'   => $request->get_param('posts_per_page')
                        ,'post_status'      => 'publish'
                        ,'order'            => $request->get_param('order')
                        ,'orderby'          => 'title'
                    );

                    if($request->get_param('campus'))
                    {
                        $args['tax_query'] = array(
                            array(
                                'taxonomy'  => 'ukm-campus'
                                ,'field'    => 'name'
                                ,'terms'    => $request->get_param('campus')
                            )
                        );
                    }
                    
                    if($request->get_param('get_total'))
                    {
                        $args['no_found_post'] = true;

                        $query  = new WP_Query($args);

                        return rest_ensure_response(array( 'code' => 200, 'total' => $query->found_post ));
                    }
                    else
                    {
                        $query  = new WP_Query($args);

                        $clubs   = array();

                        while($query->have_posts(  ))
                        {
                            $query->the_post();

                            $clubs[] = array(
                                'name'      => get_the_title()
                                ,'website'  => get_post_meta(get_the_ID(),'_post_organization_url', true)
                                ,'logo'     => get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' )
                            );
                        }

                        return rest_ensure_response(array( 'code' => 200, 'club' => $clubs ));
                    }

                }
                ,'permission_callback'  => array($this,'service_access')
                ,'args'                 => array(
                    'posts_per_page'    => array(
                        'description'   => 'Number of feed to get'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 10
                    )
                    ,'order'            => array(
                        'description'   => 'Feed sort direction'
                        ,'type'         => 'string'
                        ,'required'     => false
                        ,'default'      => 'DESC'
                    )
                    ,'paged'    => array(
                        'description'   => 'pagination parametr'
                        ,'type'         => 'int'
                        ,'required'     => false
                        ,'default'      => 1
                    )
                    ,'get_total'    => array(
                        'description'   => 'Get result or the total of the feed'
                        ,'type'         => 'bool'
                        ,'required'     => false
                        ,'default'      => false
                    )
                    ,'campus'    => array(
                        'description'   => 'Filter By Campus'
                        ,'required'     => false
                        ,'default'      => false
                    )
                )
            )
        ));
    }

    /**
     * Register student organization
     */
    function organization(){
        switch(current_action(  ))
        {
            case 'init':
                // Add Student Association Post Type
                $labels_student_association = array(
                    'name' => _x('Student Associations', 'post type general name'),
                    'singular_name' => _x('Student Association', 'post type singular name'),
                    'add_new' => _x('Add New Student Association', 'designer'),
                    'add_new_item' => __('Add New Student Association'),
                    'edit_item' => __('Edit Student Association'),
                    'new_item' => __('New Student Association'),
                    'view_item' => __('View Student Associations'),
                    'search_items' => __('Search Student Associations'),
                    'not_found' => __('No Student Association found'),
                    'not_found_in_trash' => __('No Student Association found in Trash'),
                    'parent_item_colon' => ''
                );
                $args_student_association = array(
                    'labels' => $labels_student_association,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug'  => 'student-association'
                    ),
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'menu_position' => 5,
                    'has_archive' => true,
                    'supports' => array('title', 'thumbnail')
                    ,'menu_icon'    => 'dashicons-businessperson'
                );
                register_post_type('student-association', $args_student_association);
        
               // Add Student Activity Unit Post Type
                $labels_student_activity_unit = array(
                    'name' => _x('Student Activity Units', 'post type general name'),
                    'singular_name' => _x('Student Activity Unit', 'post type singular name'),
                    'add_new' => _x('Add New Student Activity Unit', 'designer'),
                    'add_new_item' => __('Add New Student Activity Unit'),
                    'edit_item' => __('Edit Student Activity Unit'),
                    'new_item' => __('New Student Activity Unit'),
                    'view_item' => __('View Student Activity Units'),
                    'search_items' => __('Search Student Activity Units'),
                    'not_found' => __('No Student Activity Unit found'),
                    'not_found_in_trash' => __('No Student Activity Unit found in Trash'),
                    'parent_item_colon' => ''
                );
                $args_student_activity_unit = array(
                    'labels' => $labels_student_activity_unit,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug'  => 'student-activity-unit'
                    ),
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'menu_position' => 5,
                    'has_archive' => true,
                    'supports' => array('title', 'thumbnail')
                    ,'menu_icon'    => 'dashicons-universal-access'
                );
                register_post_type('studentactivityunit', $args_student_activity_unit);
        
                // Add Student Community Post Type
                $labels_student_community = array(
                    'name' => _x('Student Communities', 'post type general name'),
                    'singular_name' => _x('Student Community', 'post type singular name'),
                    'add_new' => _x('Add New Student Community', 'designer'),
                    'add_new_item' => __('Add New Student Community'),
                    'edit_item' => __('Edit Student Community'),
                    'new_item' => __('New Student Community'),
                    'view_item' => __('View Student Communities'),
                    'search_items' => __('Search Student Communities'),
                    'not_found' => __('No Student Community found'),
                    'not_found_in_trash' => __('No Student Community found in Trash'),
                    'parent_item_colon' => ''
                );
                $args_student_community = array(
                    'labels' => $labels_student_community,
                    'public' => true,
                    'publicly_queryable' => true,
                    'show_ui' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug'  => 'student-community'
                    ),
                    'capability_type' => 'post',
                    'hierarchical' => false,
                    'menu_position' => 5,
                    'has_archive' => true,
                    'supports' => array('title', 'editor')
                    ,'menu_icon'    => 'dashicons-groups'
                );
                register_post_type('student-community', $args_student_community);

                register_taxonomy( 'ukm-campus', array('student-association', 'studentactivityunit', 'student-community'), array(
                    'show_in_menu'          => true
                    ,'show_in_nav_menus'    => false
                    ,'label'                => 'Campuses'
                ) );

                binus_inside_post_element_enable('is_singular', array('student-community'), array(
                    'usedate'           => false
                    ,'usesharer'        => false
                    ,'usecomment'       => false
                    ,'uselastupdate'    => false
                    ,'userelated'       => false
                    ,'useauthor'        => false
                    ,'usecover'         => false
                ));
                binus_inside_post_add_element('is_singular', array('student-community'), array(
                    'before_title'      => function(){
                        ?>
                        <span class="image-box contain boxed square block-centering box-200" id="community-logo">
                            <?php the_post_thumbnail( 'full' ) ?>
                        </span>
                        <?php
                    }
                    ,'after_title'      => function(){
                        ?>
                        <div id="community-information">
                            <div class="info-wrapper">
                                <span class="meta-text">YEAR FOUNDED</span>
                                <span class="info"><?php echo get_post_meta( get_the_ID(), '_founded', true ) ?></span>
                            </div>
                            <div class="info-wrapper">
                                <?php $web_address = get_permalink(); ?>
                                <span class="meta-text">LINK ADDRESS</span>
                                <span class="info">
                                    <a href="<?php echo $web_address ?>"><?php echo $web_address ?></a>
                                </span>
                            </div>
                            <div class="info-wrapper">
                                <span class="meta-text">CONTACT PERSON</span>
                                <span class="info"><?php echo get_post_meta( get_the_ID(), '_cs_email', true ) ?></span>
                            </div>
                            <div class="info-wrapper">
                                <span class="meta-text">STAY CONNECTED</span>
                                <ul id="community-socmeds" class="clear">
                                    <?php
                                    $socmeds = get_post_meta( get_the_ID(), '_social_media', true );
                                    if(empty($socmeds))
                                    {
                                        $socmeds = array(array());
                                    }
                                    foreach (array(
                                        '_tw'    => 'twitter'
                                        ,'_ig'   => 'instagram'
                                        ,'_fb'   => 'facebook'
                                        ,'_yt'   => 'youtube'
                                    ) as $key => $label) 
                                    {
                                        if(empty($socmeds[0][$key]))
                                        {
                                            continue;
                                        }

                                        printf(
                                            '<li class="socmed-item socmed-%2$s"><a class="the-icon" href="%1$s">%3$s</a></li>'
                                            ,$socmeds[0][$key]
                                            ,$key
                                            ,bhelp()->svg($label,'',false)
                                        );
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                    }
                ));
                break;
            case 'pre_get_posts':
                $query = func_get_arg(0);
                if(
                    !is_admin()
                    AND
                    (
                    is_post_type_archive( 'student-association' )
                    OR
                    is_post_type_archive( 'studentactivityunit' )
                    OR
                    is_post_type_archive( 'student-community' )
                    )
                )
                {
                    $query->set('posts_per_page', 20);
                    $query->set('order', 'ASC');
                    $query->set('orderby', 'meta_value');
                    $query->set('meta_key', '_post_organization_url');
                }
                break;
            case 'cmb2_admin_init':
                new_cmb2_box(array(
                    'id'            => 'student-comm-meta'
                    ,'title'        => 'Community Information'
                    ,'object_types' => array('student-community')
                    ,'option_key'   => 'student-comm-meta'
                    ,'capability'   => 'edit_pages'
                    ,'fields'       => array(
                        array(
                            'id'        => '_thumbnail'
                            ,'type'     => 'file'
                            ,'name'     => 'Community Logo'
                            ,'options' => array(
                                'url' => false
                            )
                            ,'text'    => array(
                                'add_upload_file_text' => 'Add Logo'
                            )
                            ,'query_args' => array(
                                'type' => array(
                                	'image/gif'
                                	,'image/jpeg'
                                	,'image/jpg'
                                	,'image/png'
                                )
                            )
                        )
                        ,array(
                            'id'        => '_founded'
                            ,'type'     => 'text_small'
                            ,'name'     => 'Year founded'
                        )
                        ,array(
                            'id'        => '_cs_email'
                            ,'type'     => 'text_email'
                            ,'name'     => 'Contact Person Emial'
                        )
                        ,array(
                            'id'        => '_social_media'
                            ,'type'     => 'group'
                            ,'name'     => 'Social Media'
                            ,'fields'   => array(
                                array(
                                    'id'        => '_tw'
                                    ,'type'     => 'text_url'
                                    ,'name'     => 'Twitter account URL'
                                )
                                ,array(
                                    'id'        => '_fb'
                                    ,'type'     => 'text_url'
                                    ,'name'     => 'Facebook page URL'
                                )
                                ,array(
                                    'id'        => '_ig'
                                    ,'type'     => 'text_url'
                                    ,'name'     => 'Instagram account URL'
                                )
                                ,array(
                                    'id'        => '_yt'
                                    ,'type'     => 'text_url'
                                    ,'name'     => 'YouTube account URL'
                                )
                            )
                        )
                    )
                ));
                new_cmb2_box(array(
                    'id'            => 'student-org-meta'
                    ,'title'        => 'Community Information'
                    ,'object_types' => array('studentactivityunit', 'student-association')
                    ,'option_key'   => 'student-org-meta'
                    ,'capability'   => 'edit_pages'
                    ,'fields'       => array(
                        array(
                            'id'        => '_post_organization_url'
                            ,'type'     => 'text_url'
                            ,'name'     => 'Organization website URL'
                        )
                    )
                ));
                break;

        }
    }

    /**
     * Add custom web config
     */
    function web_config($backend){
        $backend->add_field(array(
            'name'      => 'First Block Category'
            ,'id'       => 'first-block-cat'
            ,'type'     => 'select_category'
        ));
        $backend->add_field(array(
            'name'      => 'Second Block Category'
            ,'id'       => 'second-block-cat'
            ,'type'     => 'select_category'
        ));
    }

    function display_student_gruoups($query, $is_archive = false){
        global $post;
        if($query->have_posts())
        {
            echo '<ul class="student-group-wrapper row">';
            while($query->have_posts())
            {
                $query->the_post();
                $address = get_post_meta(get_the_ID(),'_post_organization_url', true);
                if(empty($address))
                {
                    $address = get_permalink(  );
                }
                ?>
                <li class="student-group col-xs-6 col-sm-6 col-md-4">
                    <a href="<?php echo $address ?>" target="blank" class="image-box square box-100 boxed block-centering">
                        <img class="deffer" data-src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'thumbnail' ) ?>" alt="<?php get_the_title() ?>">
                    </a>
                </li>
                <?php
            }
            echo '</ul>';
            if(!$is_archive)
            {
                printf('&nbsp;<p class="view-all"><a href="%s">View All</a></p>',get_post_type_archive_link( $post->post_type  ));
            }
            else
            {
                binus_template(
                    'pagination'
                    ,$is_archive
                    ,$query->max_num_pages
                    ,max(1, get_query_var( 'paged', 1 ))
                    ,array()
                    ,'page/%#%'
                );
            }
        }
    }
}

/**
 * Get child theme engine
 * 
 * @return univ the class instance 
 */
function scac(){
    ob_start();
    return scac::get_instance();
}

scac()->main();

/**
 * Register configuration
 */
binus_theme_config(array(scac(),'web_config'));

?>