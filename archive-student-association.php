<?php get_header() ?>
<?php binus_template('container2_open', 'student-groups-archive') ?>
    <?php 
    binus_template('breadcrumb', array(
        array(
            home_url(),get_bloginfo()
        )        
        ,'Student Association'
    )); 
    ?>
    <?php binus_template('block_header', 'Student Association') ?>
    <?php
    global $wp_query; 
    scac()->display_student_gruoups($wp_query, get_post_type_archive_link( $wp_query->query['post_type'] )); ?>
<?php binus_template('container2_close', 'student-groups-archive') ?>
<?php get_footer() ?>