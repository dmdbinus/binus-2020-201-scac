<?php get_header() ?>
<?php binus_template('slideshow', array('type' => 'type-three')) ?>

<?php binus_template('container2_open', 'first-block') ?>
    <?php
    $cat = binus_get_settings('first-block-cat', 1, true);
    binus_template('post_roll', array(
        'container_id'      => 'first-block-posts'
        ,'category'         => $cat
        ,'type'             => 'type-10'
        ,'posts_per_page'   => 3
        ,'block_icon'       => bhelp()->svg('icon-newspaper', '', false)
        ,'setting_id'       => 'first-block-cat'
        ,'no_found_rows'    => true
    ));
    ?>
<?php binus_template('container2_close', 'first-block') ?>

<?php binus_template('container2_open', 'home-news-event', 'home-row'); ?>
    <div class="row">
        <div class="col-xs-12 col-md-6 home-column">
            <div class="block-centering col-sm-mobile-width">
                <?php 
                $cat = binus_get_settings('second-block-cat', 1);
                binus_template('post_roll', array(
                    'container_id'      => 'second-block-posts'
                    ,'category'         => $cat
                    ,'type'             => 'type-five'
                    ,'posts_per_page'   => 3
                    ,'block_icon'       => bhelp()->svg('icon-paper', '', false)
                    ,'setting_id'       => 'second-block-cat'
                    ,'no_found_rows'    => true
                ));
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 home-column">
            <?php 
            binus_template('events', array(
                'block_icon'           => bhelp()->svg_image('icon-calendar', '', false)
                ,'type'                 => 'type-three'
            )); 
            ?>
        </div>
    </div>
<?php binus_template('container2_close'); ?>

<?php binus_template('combination_fs_bp') ?>

<?php binus_template('container2_open', 'home-award') ?>
    <?php
    binus_template('award', array(
        'id'            => 'binusachievedaward'
        ,'block_title'  => 'Student Award'
    ));
    ?>
<?php binus_template('container2_close') ?>

<?php binus_template('container2_open', 'home-gallery') ?>
    <?php
    binus()->render_core_template('post-roll/binus-2017/type-17',array(
        'posts'                 => binus_component_post_roll::get_instance()->prepare_data(new WP_Query(array(
            'post_type'         => 'gallery'
            ,'posts_per_page'   => 9
            ,'post_status'      => 'publish'
        )), array(
            'type'  => 'type-17'
        ))
        ,'block_title'          => 'Gallery'
        ,'block_icon'           => bhelp()->svg('icon-image', '', false)
        ,'block_description'    => ''
        ,'not_found_message'    => 'No Image Found'
        ,'view_all_text'        => 'View All'
        ,'view_all_url'         => get_post_type_archive_link( 'gallery' )
        ,'category'             => 0
        ,'component_id'         => 'home-gall'
        ,'is_archive'           => false
    ));
    ?>
<?php binus_template('container2_close', 'home-gallery') ?>

<?php binus_template('container2_open', 'student-groups') ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <?php binus_template('block_header', 'Student Association') ?>
            <?php
            $assoc = new WP_Query(array(
                'post_status'       => 'publish'
                ,'posts_per_page'   => 9
                ,'post_type'        => 'student-association'
                ,'order'            => 'RAND'
            ));
            scac()->display_student_gruoups($assoc);
            ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <?php binus_template('block_header', 'Student Activity Unit') ?>
            <?php
            $activity = new WP_Query(array(
                'post_status'       => 'publish'
                ,'posts_per_page'   => 9
                ,'post_type'        => 'studentactivityunit'
                ,'order'            => 'RAND'
            ));
            scac()->display_student_gruoups($activity);
            ?>
        </div> 
    </div>
    <div id="student-community">
        <?php binus_template('block_header', 'Student Community') ?>
        <?php
        $community = new WP_Query(array(
            'post_status'       => 'publish'
            ,'posts_per_page'   => 6
            ,'post_type'        => 'student-community'
            ,'order'            => 'RAND'
        ));
        scac()->display_student_gruoups($community);
        ?>
    </div>
<?php binus_template('container2_close', 'student-groups') ?>



<?php get_footer() ?>